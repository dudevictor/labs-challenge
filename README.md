# Quake log parser

Parser do Log do Quake 3 Arena. 
Implementado utilizando NodeJS com ExpressJS para servir a API e 
Mocha + Chai para implementar a suíte de testes.

Link no Heroku:
https://quake3-log-parser.herokuapp.com/games/


#### Passos para o setup do projeto

1. `git clone https://gitlab.com/dudevictor/labs-challenge.git`
2. `npm install`
3. `npm start`

A aplicação estará disponível em http://localhost:3000
com duas rotas habilitadas: `/games` e `/games/:gameNum`, 
onde `gameNum` é o número do jogo (1 a 21) 

Para executar a suíte de testes execute `npm test` com a aplicação em execução 

#### Detalhes sobre a implementação
A implementação realiza a leitura dos logs de Quake 3 Arena e identifica os comandos de 
início, fim de jogo e kills. Os comandos são identificados através de Regex e estão apresentados
no arquivo `/src/business/CommandIdentifier.js`. 

O processamento do progresso do jogo conforme as linhas vão sendo lidas é registrada 
na classe `GameContext`. Já a classe `GameLogProcessor` é a classe de mais alto nível da camada de negócio, e é 
responsável por coordenar 
o fluxo de dados da leitura do arquivo com o registro dos progressos dos jogos. 

Os dados dos jogos que são retornados pela API foram mapeados na classe `Game`.

