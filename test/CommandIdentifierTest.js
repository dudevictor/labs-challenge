let expect  = require('chai').expect;
const { CommandIdentifier, GAME_COMMANDS } = require('../src/business/CommandIdentifier');

describe('Unit tests for CommandIdentifier', () => {

    it('Identify Initializing new Game', (done) => {
        let command = CommandIdentifier.identifyCommand("  0:00 InitGame: \\sv_floodProtect\\1\\sv_maxPing\\0\\...");

        expect(command).eql(GAME_COMMANDS.INIT_GAME.name);
        done();
    })

    it('Identify User Connected', (done) => {
        let command = CommandIdentifier.identifyCommand(" 20:38 ClientUserinfoChanged: 2 n\\Isgalamido\\....");

        expect(command).eql(GAME_COMMANDS.USER_CONNECTED.name);
        done();
    })

    it('Identify Kill', (done) => {
        let command = CommandIdentifier.identifyCommand(" 20:54 Kill: 1022 2 22: Dono da bola killed Isgalamido by MOD_TRIGGER_HURT\n");

        expect(command).eql(GAME_COMMANDS.KILL.name);
        done();
    })

    it('Identify Shutdown Game', (done) => {
        let command = CommandIdentifier.identifyCommand("  20:37 ShutdownGame:\n");

        expect(command).eql(GAME_COMMANDS.SHUTDOWN_GAME.name);
        done();
    })

    it('Command not identified', (done) => {
        let command = CommandIdentifier.identifyCommand(" 20:54 Example of an inexistent command");

        expect(command).to.be.an('undefined');
        done();
    })

})

