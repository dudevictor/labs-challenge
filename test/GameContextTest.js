const expect  = require('chai').expect;
const { GameContext } = require('../src/business/GameContext');
const { Game } = require('../src/model/Game');
const { GAME_COMMANDS } = require('../src/business/CommandIdentifier');

describe('Unit tests for GameContext', () => {

    it('Test world kills', (done) => {

        let context = new GameContext();
        context.analyseCommand(GAME_COMMANDS.INIT_GAME.name, '   0:00 InitGame:');
        context.analyseCommand(GAME_COMMANDS.USER_CONNECTED.name, ' 20:38 ClientUserinfoChanged: 2 n\\Isgalamido\\t\\');
        context.analyseCommand(GAME_COMMANDS.KILL.name, ' 20:54 Kill: 1022 2 22: <world> killed Isgalamido by MOD_TRIGGER_HURT\n');
        context.analyseCommand(GAME_COMMANDS.KILL.name, ' 21:07 Kill: 1022 2 22: <world> killed Isgalamido by MOD_TRIGGER_HURT\n');
        context.analyseCommand(GAME_COMMANDS.SHUTDOWN_GAME.name, ' 20:37 ShutdownGame:');

        let game = context.games[0];
        expect(game).eql(new Game(
            2, ['Isgalamido'], { 'Isgalamido' : -2 }
        ));
        done();
    })

    it('Test two players no kills', (done) => {

        let context = new GameContext();
        context.analyseCommand(GAME_COMMANDS.INIT_GAME.name, '   0:00 InitGame:');
        context.analyseCommand(GAME_COMMANDS.USER_CONNECTED.name, ' 20:38 ClientUserinfoChanged: 2 n\\Isgalamido\\t\\');
        context.analyseCommand(GAME_COMMANDS.USER_CONNECTED.name, ' 20:38 ClientUserinfoChanged: 2 n\\Mocinha\\t\\');
        context.analyseCommand(GAME_COMMANDS.SHUTDOWN_GAME.name, ' 20:37 ShutdownGame:');

        let game = context.games[0];
        expect(game).eql(new Game(
            0, ['Isgalamido', 'Mocinha'], { 'Isgalamido' : 0, 'Mocinha' : 0 }
        ));
        done();
    })

    it('Test suicide', (done) => {

        let context = new GameContext();
        context.analyseCommand(GAME_COMMANDS.INIT_GAME.name, '   0:00 InitGame:');
        context.analyseCommand(GAME_COMMANDS.USER_CONNECTED.name, ' 20:38 ClientUserinfoChanged: 2 n\\Isgalamido\\t\\');
        context.analyseCommand(GAME_COMMANDS.USER_CONNECTED.name, ' 20:38 ClientUserinfoChanged: 2 n\\Mocinha\\t\\');
        context.analyseCommand(GAME_COMMANDS.KILL.name, ' 10:53 Kill: 3 3 7: Isgalamido killed Isgalamido by MOD_ROCKET_SPLASH\n');
        context.analyseCommand(GAME_COMMANDS.SHUTDOWN_GAME.name, ' 20:37 ShutdownGame:');

        let game = context.games[0];
        expect(game).eql(new Game(
            1, ['Isgalamido', 'Mocinha'], { 'Isgalamido' : 0, 'Mocinha' : 0 }
        ));
        done();
    })

    it('An example of kill', (done) => {

        let context = new GameContext();
        context.analyseCommand(GAME_COMMANDS.INIT_GAME.name, '   0:00 InitGame:');
        context.analyseCommand(GAME_COMMANDS.USER_CONNECTED.name, ' 20:38 ClientUserinfoChanged: 2 n\\Isgalamido\\t\\');
        context.analyseCommand(GAME_COMMANDS.USER_CONNECTED.name, ' 20:38 ClientUserinfoChanged: 2 n\\Mocinha\\t\\');
        context.analyseCommand(GAME_COMMANDS.KILL.name, ' 10:53 Kill: 3 3 7: Isgalamido killed Mocinha by MOD_ROCKET_SPLASH\n');
        context.analyseCommand(GAME_COMMANDS.SHUTDOWN_GAME.name, ' 20:37 ShutdownGame:');

        let game = context.games[0];
        expect(game).eql(new Game(
            1, ['Isgalamido', 'Mocinha'], { 'Isgalamido' : 1, 'Mocinha' : 0 }
        ));
        done();
    })

    it('An example of game', (done) => {

        let context = new GameContext();
        context.analyseCommand(GAME_COMMANDS.INIT_GAME.name, '   0:00 InitGame:');
        context.analyseCommand(GAME_COMMANDS.USER_CONNECTED.name, ' 20:38 ClientUserinfoChanged: 2 n\\Isgalamido\\t\\');
        context.analyseCommand(GAME_COMMANDS.KILL.name, ' 20:54 Kill: 1022 2 22: <world> killed Isgalamido by MOD_TRIGGER_HURT\n');
        context.analyseCommand(GAME_COMMANDS.KILL.name, ' 21:07 Kill: 1022 2 22: <world> killed Isgalamido by MOD_TRIGGER_HURT\n');
        context.analyseCommand(GAME_COMMANDS.USER_CONNECTED.name, '  21:15 ClientUserinfoChanged: 2 n\\Isgalamido\\t\\');
        context.analyseCommand(GAME_COMMANDS.USER_CONNECTED.name, ' 21:51 ClientUserinfoChanged: 3 n\\Dono da Bola\\t\\');
        context.analyseCommand(GAME_COMMANDS.KILL.name, '  22:18 Kill: 2 2 7: Isgalamido killed Isgalamido by MOD_ROCKET_SPLASH\n');
        context.analyseCommand(GAME_COMMANDS.KILL.name, '  22:18 Kill: 2 2 7: Isgalamido killed Isgalamido by MOD_ROCKET_SPLASH\n');
        context.analyseCommand(GAME_COMMANDS.KILL.name, '  22:18 Kill: 2 2 7: Dono da Bola killed Isgalamido by MOD_ROCKET_SPLASH\n');
        context.analyseCommand(GAME_COMMANDS.SHUTDOWN_GAME.name, ' 20:37 ShutdownGame:');

        let game = context.games[0];
        expect(game).eql(new Game(
            5, ['Isgalamido', 'Dono da Bola'], { 'Isgalamido' : -2, 'Dono da Bola' : 1 }
        ));
        done();
    })

    it('An example with consecutive init game calls', (done) => {

        let context = new GameContext();
        context.analyseCommand(GAME_COMMANDS.INIT_GAME.name, '   0:00 InitGame:');
        context.analyseCommand(GAME_COMMANDS.INIT_GAME.name, '   0:00 InitGame:');
        context.analyseCommand(GAME_COMMANDS.SHUTDOWN_GAME.name, '   0:00 InitGame:');

        let gamesCount = context.games.length;
        expect(gamesCount).eql(2);
        done();
    })

})

