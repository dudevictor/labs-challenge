const expect  = require('chai').expect;
const path  = require('path');
const { GameLogProcessor } = require('../src/business/GameLogProcessor');
const { Game } = require('../src/model/Game');

describe('Integration tests of GameLogProcessor', () => {

    it('Log test case 1', (done) => {

        let processor = new GameLogProcessor( path.resolve(__dirname, './logsTest/example1.log'));
        processor.processLogFile();

        expect(processor.games).eql(
            { game_1 : new Game(60,
                    ["Oootsimo",
                        "Dono da Bola",
                        "Zeh",
                        "Chessus",
                        "Mal",
                        "Assasinu Credi",
                        "Isgalamido"],

                    { "Oootsimo": -1,
                        "Dono da Bola": 3,
                        "Zeh": 7,
                        "Chessus": 5,
                        "Mal": 1,
                        "Assasinu Credi": 3,
                        "Isgalamido": 5 }
                )});
        done();
    })

    it('Log test case 2', (done) => {

        let processor = new GameLogProcessor( path.resolve(__dirname, './logsTest/example2.log'));
        processor.processLogFile();

        expect(processor.games).eql(
            { game_1 : new Game(122,
                    ["Isgalamido",
                        "Dono da Bola",
                        "Zeh",
                        "Oootsimo",
                        "Chessus",
                        "Assasinu Credi",
                        "Mal"],

                    { "Assasinu Credi": 3,
                        "Chessus": 7,
                        "Dono da Bola": 1,
                        "Isgalamido": 22,
                        "Mal": -5,
                        "Oootsimo": 9,
                        "Zeh": 4 },
                )});
        done();
    })

})

