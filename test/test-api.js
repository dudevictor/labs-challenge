let expect  = require('chai').expect;
let request = require('request');

const host = 'http://localhost:3000';

describe('Suite tests for api', () => {

    it('Test /games route', (done) => {
        request(`${host}/games` , (error, response, body) => {
            expect(body).to.not.be.empty;
            done();
        });
    });

    it('Test "/" not found route', (done) => {
        request(`${host}/` , (error, response, body) => {
            expect(response.statusCode).eql(404);
            expect(JSON.parse(response.body)).eql({
                status : 404,
                error : "The requested route was not found"
            });
            done();
        });
    });

    it('Test "/games/:gameNum" route', (done) => {
        request(`${host}/games/1` , (error, response, body) => {
            expect(response.statusCode).eql(200);
            expect(JSON.parse(response.body)).eql({
                game_1 : { total_kills : 0,  players :["Isgalamido"], kills :{"Isgalamido" : 0}}
            });
            done();
        });
    });

    it('Test not found game num', (done) => {
        request(`${host}/games/22` , (error, response, body) => {
            expect(response.statusCode).eql(404);
            done();
        });
    });

    it('Test invalid game num', (done) => {
        request(`${host}/games/aa` , (error, response, body) => {
            expect(response.statusCode).eql(400);
            done();
        });
    });


})


