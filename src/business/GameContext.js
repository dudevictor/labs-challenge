const { Game } = require('../model/Game');
const { GAME_COMMANDS } = require('./CommandIdentifier');

/**
 * This class analyses the commands of a Quake 3 Arena and register the context of games
 *
 * It contains the following attributes:
 * GameContext.games -> The list of games registered
 * GameContext.currentGame -> the current game that is in progress in log. If no game exists, it is null.
 */
class GameContext {

    constructor() {
        this.games = [];
        this.currentGame = null;
    }

    /**
     * Analyse a {@link GAME_COMMANDS} and register the game information.
     * Call it consecutively to update the games array
     * @param commandName the identifier of a {@link GAME_COMMANDS}
     * @param line line that is represented by the {@param commandName}
     */
    analyseCommand(commandName, line) {

        switch (commandName) {

            case GAME_COMMANDS.INIT_GAME.name:
                this._pushCurrentGameIfExists();
                this.currentGame = new Game();

                break;

            case GAME_COMMANDS.SHUTDOWN_GAME.name:
                this._pushCurrentGameIfExists();
                this.currentGame = null;
                break;

            case GAME_COMMANDS.USER_CONNECTED.name:
                this._processUserConnection(line);
                break;

            case GAME_COMMANDS.KILL.name:
                this._processKill(line);
                break;

            default:
                console.error(`Case for command '${commandName}' was not implemented`);

        }

    }

    /**
     * The current Game will be pushed in games array if its defined
     * @private
     */
    _pushCurrentGameIfExists() {
        if (this.currentGame) {
            this.games.push(this.currentGame);
        }
    }

    /**
     * Logic for {@link GAME_COMMANDS.USER_CONNECTED}.
     * Register the user in current game if it was not registered yet
     * @param line the line that contains the information
     * @private
     */
    _processUserConnection(line) {
        const regexToExtractUserName = /ClientUserinfoChanged: [0-9]+ n\\(.*?)\\t/;
        const username = line.match(regexToExtractUserName)[1];

        if (this.currentGame.kills[username] === undefined) {
            this.currentGame.players.push(username);
            this.currentGame.kills[username] = 0;
        }

    }

    /**
     * Logic for {@link GAME_COMMANDS.KILL}.
     * Regsiter the kills in current game
     * @param line the line that contins the information
     * @private
     */
    _processKill(line) {
        const regexToExtractKill = /Kill: [0-9]+ [0-9]+ [0-9]+: (.*?) killed (.*?) by/;
        const match = line.match(regexToExtractKill);
        const playerMurderer = match[1];
        const playerKilled = match[2];

        this._computeNewKill(playerMurderer, playerKilled);
    }


    /**
     * Compute and updates the kills in current game
     * @param playerMurderer name of player that murdered a player
     * @param playerKilled name of killed player
     * @private
     */
    _computeNewKill(playerMurderer, playerKilled) {
        const WORLD = '<world>';
        this.currentGame.total_kills += 1;

        if (playerMurderer === WORLD) {
            this.currentGame.kills[playerKilled] -= 1;
        } else if (playerMurderer !== playerKilled) {
            this.currentGame.kills[playerMurderer] += 1;
        }
    }

}

module.exports = { GameContext };
