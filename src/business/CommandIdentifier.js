/**
 * Defines the list of known commands that is proccessed by the application
 * Each of them defines its name (an identifier) and a regex that can be used to match the line of log that represents the command
 */
const GAME_COMMANDS = {
    INIT_GAME : {
        name: 'INIT_GAME',
        regex : /^( )*[0-9]+:[0-9]+ InitGame:/g, // Pattern: '##:## InitGame:'
    },
    SHUTDOWN_GAME : {
        name: 'SHUTDOWN_GAME',
        regex : /^( )*[0-9]+:[0-9]+ ShutdownGame:/g, // Pattern: '##:## ShutdownGame:'
    },
    USER_CONNECTED : {
        name: 'USER_CONNECTED',
        regex : /^( )*[0-9]+:[0-9]+ ClientUserinfoChanged:/g // Pattern: '##:## ClientUserInfoChanged
    },
    KILL : {
        name: 'KILL',
        regex: /^( )*[0-9]+:[0-9]+ Kill:/g // Pattern ''##:## Kill:'
    }
};

/**
 * It is a utility class that helps to identify which {@link GAME_COMMANDS} represents a line
 */
class CommandIdentifier {

    /**
     * Tell which {@link GAME_COMMANDS} a line is represented.
     * If the line was not known by the listed commands, undefined is returned.
     * @param line the line that will be interpreted
     * @returns {string} returns the identifier of {@link GAME_COMMANDS} that the line represents, otherwise undefined is returned
     */
    static identifyCommand(line) {

        return Object.keys(GAME_COMMANDS).find(key => {
            let command = GAME_COMMANDS[key];
            return line.match(command.regex);
        });

    }

}

module.exports = { CommandIdentifier, GAME_COMMANDS };
