const fs = require('fs');
const path = require('path');
const { GameContext } = require('./GameContext');
const { CommandIdentifier } = require('./CommandIdentifier');

/**
 * This class reads a log file of Quake 3 arena and can retrieve game data information
 */
class GameLogProcessor {

    /**
     * Initialize a new instance of GameLogProcessor
     * @param logPath the path to log of Quake 3 arena
     */
    constructor(logPath = path.resolve(__dirname, '../assets/games.log')) {
        this.logPath = logPath;
        this.gameContext = new GameContext();
    }

    /**
     * Process the log file and load game data information.
     * Call it after initialize the instance.
     */
    processLogFile() {
        let lines = fs.readFileSync(this.logPath).toString().split('\n');
        this._processLines(lines)
    }

    _processLines(lines) {

        lines.forEach((line) => {
            let command = CommandIdentifier.identifyCommand(line);

            if (command) {
                this.gameContext.analyseCommand(command, line);
            }

        });
    }

    /**
     * Get the games data information
     * @returns an object in following format:
     *  { game_# : {(Game Instance},
     *    ...
     *  }
     */
    get games() {
        return this.gameContext.games.map((game, index) => {
            return { [`game_${index + 1}`]: game }
        }).reduce((previous, current) => {
            return { ...previous, ...current }
        });
    }
}

module.exports = { GameLogProcessor };
