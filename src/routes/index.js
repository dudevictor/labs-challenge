let express = require('express');
let { GameLogProcessor } = require('../business/GameLogProcessor');
let router = express.Router();

let processor = new GameLogProcessor();
processor.processLogFile();

/**
 * Get the list of game data
 */
router.get('/games', (req, res) => {
  let response = processor.games;
  res.send(response)
});

/**
 * Get game data from its num
 */
router.get('/games/:gameNum', (req, res) => {
  let {gameNum} = req.params;

  if (validateGameNumParam(gameNum, res)) {
    let entryGame = Object.entries(processor.games)[gameNum - 1];
    res.send({ [entryGame[0]] : entryGame[1] });
  }

});

/**
 * Validate /games/:gameNum params route
 * @param gameNum number of game stating at 0
 * @param res response object
 * @returns {boolean} return true if the param is valid
 */
function validateGameNumParam(gameNum, res) {
  let valid = true;
  if (gameNum === undefined || gameNum < 1 || gameNum > Object.keys(processor.games).length) {
    res.status(404).send({ status : 404, error : 'The requested game\'s data was not found' })
    valid = false;
  }

  if (Number.isNaN(Number(gameNum))) {
    res.status(400).send({ status : 400, error : "The gameNum param is invalid" });
    valid = false;
  }

  return valid;
}

module.exports = router;
