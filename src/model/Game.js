/**
 * Represents the Game info data
 */
class Game {

    constructor(total_kills = 0, players = [], kills = {}) {
        this.total_kills = total_kills;
        this.players = players;
        this.kills = kills;
    }

}

module.exports = { Game };
